'use strict';

function ex3() {

    document.getElementById("result-div").style.display = 'block';

    var ids = [];

    collectIds(window.document, ids);

    document.getElementById("result").innerHTML = JSON.stringify(ids);
}

function collectIds(node, ids) {
    if(node.id){
        ids.push(node.id);
    }

    node.childNodes.forEach(function(each){
        collectIds(each, ids)
    });
}
