'use strict';

var express = require('express');
var cors = require('cors');
var app = express();
app.use(cors());

app.get('/api/persons/:id', getPersonById);
app.get('/api/health-insurance/:code', getHealthInsuranceInfo);
app.get('/api/tax-debt/:code', getTaxDebtInfo);

var server = app.listen(8080, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port)
});

function getHealthInsuranceInfo(req, res) {
    var code = req.params.code;

    if (code !== "A03259") {
        res.status(404);
        res.end("404 Not found");
        return;
    }


    var info = {
        person_code : "A03259",
        isInsured : true,
        insuredTill : new Date('12/29/2016')
    };

    res.set('Content-Type', 'application/json');
    res.end(JSON.stringify(info));
}

function getTaxDebtInfo(req, res) {
    var code = req.params.code;

    if (code !== "A03259") {
        res.status(404);
        res.end("404 Not found");
        return;
    }


    var info = {
        personCode : "A03259",
        debt : 100
    };

    res.set('Content-Type', 'application/json');
    res.end(JSON.stringify(info));
}

function getPersonById(req, res) {
    var id = req.params.id;

    if (id !== "2948") {
        res.status(404);
        res.end("404 Not found");
        return;
    }

    var person = {
        id : 2948,
        code : "A03259",
        name : "Jill",
        age : 20
    };

    res.set('Content-Type', 'application/json');
    res.end(JSON.stringify(person));
}
