'use strict';

function ex6() {
    $.get('http://localhost:8080/api/persons/2948', function(data) {

        var name = data.name;
        var code = data.code;

        $.get('http://localhost:8080/api/health-insurance/' + code, function(data) {
            var isInsured = data.isInsured;

            $.get('http://localhost:8080/api/tax-debt/' + code, function(data) {
                var taxDebt = data.debt;

                var person = {
                    name : name,
                    isInsured: isInsured,
                    taxDebt: taxDebt
                };

                console.log(person);
            });

        });
    });
}